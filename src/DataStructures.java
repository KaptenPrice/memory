import java.lang.reflect.Array;
import java.util.ArrayList;

public class DataStructures{
    /*Skapa en array med 10 plaster och deklarera 10 element  med  5 värdei den (int )
    Skapa en annan array med 10 platser med bara 0:or(int)

    loop
    printa ut 0 arrayen
    Gör en metod som tar in 2 gissningar på en plats (inputUser)
    Kolla inputUser mot arrayplatserna med samma värde
    Om de är samma så flyttar vi värden från första arrayen till andra arrayen på samma pos.
    Om fel, gissa igen
    Om andra arrayen blir full så bryt loopen och säg grattis
    loop

    Gör en parent klass med arrayer och getters and setters
    gör en GameKlass som äver ovanstående klass och använd setmetoderna

    Test:
    Unittest på check mot första arrayen
    unittest på om secondArr är full dvs. inte har några 0:or kvar
    unitTest på om gameklassen får tillgång till setmetoden*/
    int[] memoryArr;
    int[] guessArr;

    public int[] getMemoryArr() {
        return memoryArr;
    }

    public void setMemoryArr(int[] memoryArr) {
        this.memoryArr = memoryArr;
    }

    public int[] getGuessArr() {
        return guessArr;
    }

    public void setGuessArr(int[] guessArr) {
        this.guessArr = guessArr;
    }


}
