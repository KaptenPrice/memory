import java.util.Arrays;
import java.util.Scanner;

public class Game extends DataStructures {
    Scanner sc;
    int secondGuess;
    private int firstGuess;
    public int getFirstGuess() {
        return firstGuess;
    }

    public void setFirstGuess(int firstGuess) {
        this.firstGuess = firstGuess;
    }

    public int getSecondGuess() {
        return secondGuess;
    }

    public void setSecondGuess(int secondGuess) {
        this.secondGuess = secondGuess;
    }

    public Game() {
        System.out.println("System online");
        setArrays();
        memoryGame();
    }

    public void setArrays() {
        setMemoryArr(new int[]{1, 2, 3, 4, 5, 1, 2, 3, 4, 5});
        setGuessArr(new int[10]);

    }

    public void printArr() {
        for (int i = 0; i < getGuessArr().length; i++) {
            System.out.print(getGuessArr()[i] + " ");
        }
    }

    public void memoryGame() {

        while (true) {
            sc = new Scanner(System.in);
            printArr();
            System.out.println(" ");
            System.out.println("Give me a guess:");
            setFirstGuess(sc.nextInt());
            System.out.println("Now a second guess");
            setSecondGuess(sc.nextInt());

            if (getSecondGuess() == getFirstGuess()) {
                continue;
            }
            if (checkGuess()) {
                guessArr[getFirstGuess()] = memoryArr[getFirstGuess()];
                guessArr[getSecondGuess()] = memoryArr[getSecondGuess()];
                System.out.println("Good guess");

            } else {
                System.out.println("Hint hint " + memoryArr[getFirstGuess()] + " " + memoryArr[getSecondGuess()]);
                System.out.println("Wrong guess, try again");
            }

            if (Arrays.equals(guessArr, memoryArr)) {
                System.out.println("Congrats");
                break;
            }
        }
    }

    public boolean checkGuess() {
        return getMemoryArr()[getFirstGuess()] == getMemoryArr()[getSecondGuess()];
    }

}
